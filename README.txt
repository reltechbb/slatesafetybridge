Installing this on the PI

Prequisites:
- python3.7.4
- websocket-client

Make a directory:
/opt/SlateSafetyBridge

copy these files to the directory.

copy SlateSafetyClientBridge.service
to /lib/systemd/system

execute these commands:
sudo systemctl enable SlateSafetyClientBridge.service
sudo systemctl start SlateSafetyClientBridge.service

make sure the service is running

sudo systemctl status SlateSafetyClientBridge.service
