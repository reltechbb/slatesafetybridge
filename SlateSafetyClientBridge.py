#!/usr/bin/env python

#File header:

#/*
#* SlateSafetyBridge.py
#*
#* Copyright (c) 2001-2022 Domenix
#*
#* The file servers as a websocket to retrieve data from the vendor
#* host server and push the data it through to receivers binding to
#* to the local host address. 
#*/

import websocket
import time
import configparser
import logging
import socket
from datetime import datetime

localHost = ""
port = 8765

root_logger = logging.getLogger()
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def readPropertiesFile():
    
    config = configparser.RawConfigParser()
    config.read('config.properties')

    responses = dict(config.items('PropertiesSection'))

    return responses

def on_message(ws, message):
    try:
        s.sendto(message.encode(),("localHost",int(port)))
        #print ("Message received" + message +"\n\n")
    except Exception:
        logging.error('Error%s',exc_info=True)
        

def on_error(ws, error):
    logging.error('Error: Socket is closed.  Will attempt reconnect in 3 seconds',exc_info=True)
    time.sleep(3000)
    webSocketConnect(data['vendorurl'])
    

def on_close(ws, close_status_code, close_msg):
    logging.info('### closed ###')

def on_open(ws):
    logging.info('### open ###')

def webSocketConnect(url):
    ws = websocket.WebSocketApp(url,
        headers,
        on_open=on_open,
        on_message=on_message,
        on_error=on_error,
        on_close=on_close
        )

    ws.run_forever()



if __name__ == "__main__":

    data = readPropertiesFile()

    headers = {
        "Authorization": data['authorization'],
        "CLIENT-ID": data['client-id']
    }

    localHost = data['host']
    port = data['port']

    root_logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler('/var/ramDisk/logs/SlateSafetyBridge.log' + datetime.now().strftime("%d_%m_%Y_%H_%M")  +'.log', 'w', 'utf-8')
    root_logger.addHandler(handler)
    
    webSocketConnect(data['vendorurl'])
    
    
